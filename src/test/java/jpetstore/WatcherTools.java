package jpetstore;

import cnaf.autom.Logging;
import cnaf.autom.selenium.DriverFactoryTools;
import cnaf.autom.selenium.SeleniumTools;
import cnaf.autom.selenium.Snapshot;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

public class WatcherTools implements TestWatcher, BeforeAllCallback {

    public void beforeAll(ExtensionContext extensionContext) {
        extensionContext.getTestClass().ifPresent((aClass) -> {
            Logging.setupLogger(aClass.getSimpleName());
        });
    }

    public void testSuccessful(ExtensionContext context) {
        if (DriverFactoryTools.getDriver() != null) {
            new SeleniumTools().safeCloseDriver();
        }

    }

    public void testFailed(ExtensionContext context, Throwable cause) {
        if (DriverFactoryTools.getDriver() != null) {
            context.getTestClass().ifPresent((aClass) -> {
                Snapshot.logsAndSnaps(aClass.getSimpleName());
            });
            new SeleniumTools().safeCloseDriver();
        }
    }

    public void testAborted(ExtensionContext context, Throwable cause) {
        if (DriverFactoryTools.getDriver() != null) {
            context.getTestClass().ifPresent((aClass) -> {
                Snapshot.logsAndSnaps(aClass.getSimpleName());
            });
            new SeleniumTools().safeCloseDriver();
        }

    }
}
