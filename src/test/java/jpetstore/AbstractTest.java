package jpetstore;

import cnaf.autom.CryptTools;
import cnaf.autom.DataCollectorOTF;
import cnaf.autom.selenium.DriverFactoryTools;
import cnaf.autom.selenium.TestWatcherTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Outils;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@ExtendWith(TestWatcherTools.class)
public abstract class AbstractTest {
    final Logger LOGGER = LogManager.getLogger(AbstractTest.this);

    public static WebDriver driver;
    public static WebDriverWait wait;

    public DataCollectorOTF dataCollectorOTF = new DataCollectorOTF();
    public Outils outils = new Outils();

    public Map<String, String> parameterMap = new HashMap<>();
    public Map<String, String> jddMap = new HashMap<>();
    public Map<String, String> resultsMap = new HashMap<>();


    public String navigateur = dataCollectorOTF.mapSquashTMData("navigateur", "CHROME", parameterMap);

    @BeforeEach
    void setupDriver() throws Exception {
//        var key = CryptTools.decodeBase64ToKey(System.getenv("MA_CLEF1"));
//        DriverFactoryTools.proxyHttp = CryptTools.decrypt("MGJIvkPhDX77E7Yt86JQqmqJJeRbj7EvtEYpPwCIO8hHZa5GbUX7Ks7/uvE5mztavaeE33rTjYdowg==", key);
//        DriverFactoryTools.proxyUser = CryptTools.decrypt("fjG9OQ+4D8q3iJJq6LnDe2o5P232o2/NX9j5uz2/8UiS1NfWPr5opbW2z/P7/k4NH1PyGQ==", key);
//        DriverFactoryTools.proxyPass = CryptTools.decrypt("QA8vrSYVqkJ+vSgFXafh/vhZG3CvL5HeT5K1rQr+kiSUNC/h", key);
        DriverFactoryTools.chromeOptionsSupplier = () -> {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--remote-allow-origins=*");
            return chromeOptions;
        };
        driver = DriverFactoryTools.newDriver(navigateur);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
    }

    @AfterEach
    void collectData() throws IOException {
        dataCollectorOTF.collectTestData(this.getClass().getSimpleName(), parameterMap, jddMap, resultsMap);
    }

}
