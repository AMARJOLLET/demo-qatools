package jpetstore;

import cnaf.autom.CryptTools;
import org.junit.jupiter.api.Test;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JpetstoreKOTest extends AbstractTest {
    Map<String, String> csvMap = outils.chargementCSVJDDPourOrga(this.getClass().getSimpleName());

    private final String key = dataCollectorOTF.mapLocalData("key", csvMap.get("key"), jddMap);
    private final String userCrypt = dataCollectorOTF.mapLocalData("username", csvMap.get("username"), jddMap);
    private final String passCrypt = dataCollectorOTF.mapLocalData("password", csvMap.get("password"), jddMap);

    public JpetstoreKOTest() throws IOException {
    }

    @Test
    void testJpetstore() throws Exception {
        LOGGER.info("Début du Test");
        LOGGER.info("Pas 1 - Accéder Page accueil");
        driver.get("https://petstore.octoperf.com/");
        WelcomePage welcomePage = new WelcomePage();
        AccueilPage accueilPage = welcomePage.clickEnterTheStore(wait);

        LOGGER.info("Pas 2 - se connecter");
        ConnexionPage connexionPage = accueilPage.clickBoutonSignIn(wait);
        SecretKey secretKey = CryptTools.decodeBase64ToKey(key);
        String user = CryptTools.decrypt(userCrypt, secretKey);
        String pass = CryptTools.decrypt(passCrypt, secretKey);
        connexionPage.renseignerUserPassword(wait, user, pass);
        accueilPage = connexionPage.clickBoutonLogin(wait);

        assertEquals("can't match", accueilPage.recupWelcomeText(wait), "Le text de la page connecté n'est pas celui attendu");
    }
}
