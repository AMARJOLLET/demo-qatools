package jpetstore;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConnexionPage extends AbstractFullPage {

    @FindBy(name = "username")
    protected WebElement inputUsername;

    @FindBy(name = "password")
    protected WebElement inputPassword;

    @FindBy(name = "signon")
    protected WebElement boutonLogin;


    public void renseignerUserPassword(WebDriverWait wait, String username, String password){
        logger.info("renseigne user : '{}' et password : '{}'", username, password);
        seleniumTools.sendKeys(wait, inputUsername, username);
        seleniumTools.sendKeys(wait, inputPassword, password);
    }

    public AccueilPage clickBoutonLogin(WebDriverWait wait){
        logger.info("Click page accueil");
        seleniumTools.clickOnElement(wait, boutonLogin);
        return new AccueilPage();
    }


}
