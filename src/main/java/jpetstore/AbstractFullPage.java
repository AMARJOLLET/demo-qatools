package jpetstore;

import cnaf.autom.selenium.DriverFactoryTools;
import cnaf.autom.selenium.PageFactoryTools;
import cnaf.autom.selenium.SeleniumTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public abstract class AbstractFullPage {
    protected final Logger logger = LogManager.getLogger(AbstractFullPage.this);
    protected final WebDriver driver = DriverFactoryTools.getDriver();

    protected final SeleniumTools seleniumTools = new SeleniumTools();

    protected AbstractFullPage(){
        PageFactoryTools.initElements(driver, AbstractFullPage.this);
    }


}
