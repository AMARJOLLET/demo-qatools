package jpetstore;

import cnaf.autom.selenium.ExpectedConditionsTools;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccueilPage extends AbstractFullPage {

    @FindBy(xpath = "//a[contains(@href, 'signon')]")
    protected WebElement boutonSignIn;

    @FindBy(id = "WelcomeContent")
    protected WebElement welcomeText;

    public ConnexionPage clickBoutonSignIn(WebDriverWait wait) {
        logger.info("Click page connexion");
        seleniumTools.clickOnElement(wait, boutonSignIn);
        return new ConnexionPage();
    }

    public String recupWelcomeText(WebDriverWait wait){
        return wait.until(ExpectedConditionsTools.textNotNullInElement(welcomeText)).getText();
    }

}
