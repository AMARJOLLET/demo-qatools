package jpetstore;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WelcomePage extends AbstractFullPage {

    @FindBy(xpath = "//a[text()='Enter the Store']")
    protected WebElement boutonEnterTheStore;


    public AccueilPage clickEnterTheStore(WebDriverWait wait){
        logger.info("Click page Page Accueil");
        seleniumTools.clickOnElement(wait, boutonEnterTheStore);
        return new AccueilPage();
    }



}
