package utils;

import cnaf.autom.CsvHandler;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Outils {
    private final CsvHandler csvHandler = new CsvHandler();

    public Map<String, String> chargementCSVJDDPourOrga(String className) throws IOException {
        var csvFilePath = String.format("src/test/resources/CSV/%s.csv", className);
        List<String[]> list = csvHandler.readLinesCsv(csvFilePath);

        if (list.size() != 2) {
            throw new IllegalStateException("Le nombre de ligne du CSV n'est pas égale à 2 avec la 1ere égale au Header et la 2e égale à la Data");
        }

        return IntStream.range(0, list.get(0).length).boxed().collect(Collectors.toMap(index -> list.get(0)[index], index -> list.get(1)[index]));
    }
}
